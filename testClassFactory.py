import unittest, mysql.connector
from database import login_info
from classFactory import build_row

class DBTest(unittest.TestCase):
    
    def setUp(self):
        C = build_row("user", "id name email")
        self.c = C([1, "Steve Holden", "steve@holdenweb.com"])
        self.db = mysql.connector.Connect(**login_info)
        self.cursor = self.db.cursor()
        self.cursor.execute('drop table if exists testtable')
        self.cursor.execute('create table testtable (id integer primary key auto_increment, item varchar(200))')
        for x in [1, "Steve Holden", "steve@holdenweb.com"]:
            self.cursor.execute('insert into testtable values(%s,%s)',('',x))

    def test_attributes(self):
        self.assertEqual(self.c.id, 1)
        self.assertEqual(self.c.name, "Steve Holden")
        self.assertEqual(self.c.email, "steve@holdenweb.com")

    def test_repr(self):
        self.assertEqual(repr(self.c),
                         "user_record(1, 'Steve Holden', 'steve@holdenweb.com')")
        
    def test_retrieve(self):
        D = build_row("testtable", "id item")
        dr = D([1, "Steve Holden"])
        retrievelist = dr.retrieve(self.cursor)
        results = []
        for r in retrievelist:            
            results.append((r.id, r.item))

        self.assertEqual(results,[(1, '1'), (2, 'Steve Holden'),(3, 'steve@holdenweb.com')])
        
    def test_retrieve_with_conditions(self):
        D = build_row("testtable", "id item")
        dr = D(list( (2, 'Steve Holden')))
        results = []
        retrievelist = dr.retrieve(self.cursor, r"where item='Steve Holden'")
        for r in retrievelist:
            results.append(repr(r))
            
        self.assertEqual(results[0],repr(dr))
        self.assertEqual(1, len(results))
            
        
    
    def tearDown(self):   
        self.cursor.execute('drop table if exists testtable')     
        self.db.close()

if __name__ == "__main__":
    unittest.main()

